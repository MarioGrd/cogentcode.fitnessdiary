﻿namespace CogentCode.FitnessDiary.Infrastructure.Database
{

    using CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database;
    using CogentCode.FitnessDiary.Infrastructure.Database.Internal;
    using CogentCode.FitnessDiary.Infrastructure.Database.MessageOutbox.Repositories;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;

    public static class DependencyInjection
    {
        public static IServiceCollection AddDatabase(this IServiceCollection services, DatabaseSettings settings)
        {
            services.AddDbContext<FitnessDiaryDbContext>(opt => _ = opt.UseSqlServer(settings.ConnectionString));

            services.AddScoped<ITodoRepository, TodoRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            return services;
        }
    }

    public class DatabaseSettings
    {
        public const string Key = nameof(DatabaseSettings);

        public string ConnectionString { get; set; } = default!;
    }
}
