﻿namespace CogentCode.FitnessDiary.Infrastructure.Database.MessageOutbox.Repositories
{
    using CogentCode.FitnessDiary.Application.Contracts.Common;
    using CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database;
    using CogentCode.FitnessDiary.Domain;
    using CogentCode.FitnessDiary.Infrastructure.Database.Internal;

    using Microsoft.EntityFrameworkCore;

    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    internal sealed class TodoRepository : ITodoRepository
    {
        private readonly DbSet<Todo> _todos;

        public TodoRepository(FitnessDiaryDbContext context)
        {
            this._todos = context.Set<Todo>();
        }

        public void Create(Todo todo)
        {
            this._todos.Add(todo);
        }

        public void Delete(Todo todo)
        {
            this._todos.Remove(todo);
        }

        public async Task<List<Todo>> GetAsync(string? title, Page page, CancellationToken cancellationToken = default)
        {
            return await this._todos
                .WhereIf(title is not null, todo => todo.Title.StartsWith(title ?? string.Empty))
                .Skip(page.Skip)
                .Take(page.Take)
                .ToListAsync(cancellationToken);
        }

        public async Task<Todo?> GetByIdAsync(Guid id, CancellationToken cancellationToken = default)
        {
            return await this._todos.SingleOrDefaultAsync(todo => todo.Id == id, cancellationToken);
        }

        public async Task<Todo> GetByIdSafeAsync(Guid id, CancellationToken cancellationToken = default)
        {
            return await this.GetByIdAsync(id, cancellationToken) ?? throw new ApplicationException("Not found.");
        }

        public void Update(Todo todo)
        {
            this._todos.Update(todo);
        }
    }
}