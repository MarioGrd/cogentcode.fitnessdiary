﻿namespace CogentCode.FitnessDiary.Infrastructure.Database.Configurations
{
    using CogentCode.FitnessDiary.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    internal class TodoEntityTypeConfiguration : IEntityTypeConfiguration<Todo>
    {
        public void Configure(EntityTypeBuilder<Todo> builder)
        {
            builder.ToTable("todos");

            builder.HasKey(t => t.Id);

            builder.Property(p => p.Title).IsRequired();

            builder.Property(p => p.Description).IsRequired();

            builder.Property(p => p.Created).IsRequired();
        }
    }
}
