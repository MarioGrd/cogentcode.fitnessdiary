﻿namespace CogentCode.FitnessDiary.Infrastructure.Database.Internal
{
    using Microsoft.EntityFrameworkCore;

    using System.Reflection;

    internal class FitnessDiaryDbContext : DbContext
    {
        public FitnessDiaryDbContext(DbContextOptions<FitnessDiaryDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}
