﻿namespace CogentCode.FitnessDiary.Infrastructure.Database.Internal
{
    using CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database;

    using System.Threading;
    using System.Threading.Tasks;

    internal sealed class UnitOfWork : IUnitOfWork
    {
        private readonly FitnessDiaryDbContext _context;

        public UnitOfWork(
            FitnessDiaryDbContext context,
            ITodoRepository todos)
        {
            this._context = context;
            this.Todos = todos;
        }

        public ITodoRepository Todos { get; }

        public async Task SaveAsync(CancellationToken cancellationToken = default)
        {
            await _context.SaveChangesAsync(cancellationToken);
        }
    }
}