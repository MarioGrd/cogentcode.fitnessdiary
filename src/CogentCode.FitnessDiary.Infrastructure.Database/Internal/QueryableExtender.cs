﻿namespace CogentCode.FitnessDiary.Infrastructure.Database.Internal
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    internal static class QueryableExtender
    {
        internal static IQueryable<TEntity> WhereIf<TEntity>(
            this IQueryable<TEntity> query,
            bool condition,
            Expression<Func<TEntity, bool>> predicate) where TEntity : class
            => condition ? query.Where(predicate) : query;
    }
}
