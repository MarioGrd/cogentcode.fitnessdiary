﻿namespace CogentCode.FitnessDiary.Hub.Presentation.Api.Controllers.V1
{
    using CogentCode.FitnessDiary.Application.TodoAggregate.Commands;
    using CogentCode.FitnessDiary.Application.TodoAggregate.Queries;
    using CogentCode.FitnessDiary.Presentation.Api.Common.Constants;
    using CogentCode.FitnessDiary.Presentation.Api.Controllers;

    using MediatR;

    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    using Swashbuckle.AspNetCore.Annotations;

    using System.Net.Mime;
    using System.Threading.Tasks;

    [ApiVersion(ApiVersions.V1)]
    public class TodosController : ApiControllerBase
    {
        public TodosController(IMediator mediator) : base(mediator)
        {
        }

        [HttpGet]
        [SwaggerOperation(OperationId = nameof(GetTodos), Tags = new[] { ApiTags.Todos })]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetTodos([FromQuery] GetTodosQuery query)
        {
            return await this.ProcessAsync(query);
        }

        [HttpGet("{id}")]
        [SwaggerOperation(OperationId = nameof(GetTodo), Tags = new[] { ApiTags.Todos })]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetTodo(Guid id)
        {
            return await this.ProcessAsync(new GetTodoByIdQuery(id));
        }

        [HttpPost]
        [SwaggerOperation(OperationId = nameof(PostTodo), Tags = new[] { ApiTags.Todos })]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> PostTodo([FromBody] AddTodoCommand command)
        {
            return await this.ProcessAsync(command);
        }

        [HttpPut]
        [SwaggerOperation(OperationId = nameof(PutTodo), Tags = new[] { ApiTags.Todos })]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> PutTodo([FromBody] UpdateTodoCommand command)
        {
            return await this.ProcessAsync(command);
        }

        [HttpDelete("{id}")]
        [SwaggerOperation(OperationId = nameof(DeleteTodo), Tags = new[] { ApiTags.Todos })]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeleteTodo(Guid id)
        {
            return await this.ProcessAsync(new DeleteTodoCommand(id));
        }
    }
}