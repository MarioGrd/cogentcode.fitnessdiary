﻿namespace CogentCode.FitnessDiary.Presentation.Api.Controllers
{
    using MediatR;

    using Microsoft.AspNetCore.Mvc;

    using System.Net.Mime;
    using System.Threading.Tasks;

    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Route("api/v{version:apiVersion}/[controller]")]
    public abstract class ApiControllerBase : ControllerBase
    {
        private readonly IMediator _mediator;

        protected ApiControllerBase(IMediator mediator)
        {
            _mediator = mediator;
        }

        protected async Task<IActionResult> ProcessAsync(IRequest request)
        {
            await _mediator.Send(request);

            return NoContent();
        }

        protected async Task<IActionResult> ProcessAsync<TResult>(IRequest<TResult> request)
        {
            var result = await _mediator.Send(request);

            if (result == null)
            {
                return NoContent();
            }

            return Ok(result);
        }
    }
}