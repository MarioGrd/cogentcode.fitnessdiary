﻿namespace CogentCode.FitnessDiary.Presentation.Api.Common.Constants
{

    internal static class ApiVersions
    {
        public const string V1 = "1.0";
    }
}