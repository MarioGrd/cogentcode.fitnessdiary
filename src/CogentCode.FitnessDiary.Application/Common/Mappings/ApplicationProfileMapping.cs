﻿namespace CogentCode.FitnessDiary.Application.Common.Mappings
{
    using AutoMapper;

    using CogentCode.FitnessDiary.Application.TodoAggregate.Common;
    using CogentCode.FitnessDiary.Domain;

    internal sealed class ApplicationProfileMapping : Profile
    {
        public ApplicationProfileMapping()
        {
            this.CreateMap<Todo, TodoResponse>();
        }
    }
}