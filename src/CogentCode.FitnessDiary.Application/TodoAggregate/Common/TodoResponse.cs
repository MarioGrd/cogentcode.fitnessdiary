﻿namespace CogentCode.FitnessDiary.Application.TodoAggregate.Common
{
    using System;

    public class TodoResponse
    {
        public Guid Id { get; set; }

        public string Title { get; set; } = default!;

        public long Created { get; set; }

        public string Description { get; set; } = default!;

    }
}
