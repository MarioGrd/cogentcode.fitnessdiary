﻿namespace CogentCode.FitnessDiary.Application.TodoAggregate.Queries
{
    using AutoMapper;

    using CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database;
    using CogentCode.FitnessDiary.Application.TodoAggregate.Common;
    using CogentCode.FitnessDiary.Domain;

    using FluentValidation;

    using MediatR;

    using System;
    using System.Threading;
    using System.Threading.Tasks;

    public record class GetTodoByIdQuery(Guid Id) : IRequest<TodoResponse>;

    public class GetTodoByIdQueryValidator : AbstractValidator<GetTodoByIdQuery>
    {
        public GetTodoByIdQueryValidator()
        {
            this.RuleFor(r => r.Id)
                .NotEmpty();
        }
    }

    public class GetTodoByIdQueryHandler : IRequestHandler<GetTodoByIdQuery, TodoResponse>
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        public GetTodoByIdQueryHandler(
            IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
        }

        public async Task<TodoResponse> Handle(GetTodoByIdQuery request, CancellationToken cancellationToken)
        {
            Todo todo = await this._unitOfWork.Todos.GetByIdSafeAsync(request.Id, cancellationToken);

            return this._mapper.Map<TodoResponse>(todo);
        }
    }
}
