﻿namespace CogentCode.FitnessDiary.Application.TodoAggregate.Queries
{
    using AutoMapper;

    using CogentCode.FitnessDiary.Application.Contracts.Common;
    using CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database;
    using CogentCode.FitnessDiary.Application.TodoAggregate.Common;
    using CogentCode.FitnessDiary.Domain;

    using FluentValidation;

    using MediatR;

    using System.Threading;
    using System.Threading.Tasks;

    public record GetTodosQuery(string? Title, int? PageIndex, int? PageSize) : IRequest<List<TodoResponse>>;

    internal sealed class GetTodosQueryValidator: AbstractValidator<GetTodosQuery>
    {
        public GetTodosQueryValidator()
        {
            RuleFor(r => r.Title)
                .NotEmpty()
                .When(r => r.Title is not null);

            this.RuleFor(r => r.PageIndex)
                .GreaterThan(0)
                .When(r => r.PageIndex is not null);

            this.RuleFor(r => r.PageSize)
                .GreaterThan(0)
                .When(r => r.PageIndex is not null);
        }
    }

    internal sealed class GetTodosQueryHandler : IRequestHandler<GetTodosQuery, List<TodoResponse>>
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        public GetTodosQueryHandler(
            IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
        }

        public async Task<List<TodoResponse>> Handle(GetTodosQuery request, CancellationToken cancellationToken)
        {
            Page page = new (request.PageIndex, request.PageSize);

            List<Todo> todos = await this._unitOfWork.Todos.GetAsync(request.Title, page);

            return this._mapper.Map<List<TodoResponse>>(todos);
        }
    }

}
