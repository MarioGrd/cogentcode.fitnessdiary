﻿namespace CogentCode.FitnessDiary.Application.TodoAggregate.Commands
{
    using CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database;
    using CogentCode.FitnessDiary.Domain;

    using FluentValidation;

    using MediatR;

    using System;
    using System.Threading.Tasks;

    public record UpdateTodoCommand(Guid Id, string Title, string Description) : IRequest;

    internal sealed class UpdateTodoCommandValidator : AbstractValidator<UpdateTodoCommand>
    {
        public UpdateTodoCommandValidator()
        {
            this.RuleFor(r => r.Title)
                .NotEmpty()
                .MaximumLength(20);

            this.RuleFor(r => r.Description)
                .NotEmpty()
                .MaximumLength(100);
        }
    }

    internal sealed class UpdateTodoCommandHandler : AsyncRequestHandler<UpdateTodoCommand>
    {
        private readonly IUnitOfWork _unitOfWork;

        public UpdateTodoCommandHandler(
            IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        protected override async Task Handle(UpdateTodoCommand request, CancellationToken cancellationToken)
        {
            Todo todo = await this._unitOfWork.Todos.GetByIdSafeAsync(request.Id);

            todo.Update(request.Title, request.Description);

            this._unitOfWork.Todos.Update(todo);

            await this._unitOfWork.SaveAsync(cancellationToken);
        }
    }
}
