﻿namespace CogentCode.FitnessDiary.Application.TodoAggregate.Commands
{
    using CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database;
    using CogentCode.FitnessDiary.Domain;

    using FluentValidation;

    using MediatR;

    using System;
    using System.Threading;
    using System.Threading.Tasks;

    public record DeleteTodoCommand(Guid Id) : IRequest;

    internal sealed class DeleteTodoCommandValidator : AbstractValidator<DeleteTodoCommand>
    {
        public DeleteTodoCommandValidator()
        {
            this.RuleFor(r => r.Id)
                .NotEmpty();
        }
    }

    internal sealed class DeleteTodoCommandHandler : AsyncRequestHandler<DeleteTodoCommand>
    {
        private readonly IUnitOfWork _unitOfWork;

        public DeleteTodoCommandHandler(
            IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        protected override async Task Handle(DeleteTodoCommand request, CancellationToken cancellationToken)
        {
            Todo todo = await this._unitOfWork.Todos.GetByIdSafeAsync(request.Id, cancellationToken);

            this._unitOfWork.Todos.Delete(todo);

            await this._unitOfWork.SaveAsync(cancellationToken);
        }
    }
}
