﻿namespace CogentCode.FitnessDiary.Application.TodoAggregate.Commands
{
    using CogentCode.FitnessDiary.Application.Contracts.Common;
    using CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database;
    using CogentCode.FitnessDiary.Domain;

    using FluentValidation;

    using MediatR;

    using System.Threading;
    using System.Threading.Tasks;

    public record AddTodoCommand(string Title, string Description) : IRequest;

    internal sealed class AddTodoCommandValidator : AbstractValidator<AddTodoCommand>
    {
        public AddTodoCommandValidator()
        {
            this.RuleFor(r => r.Title)
               .MaximumLength(20)
                .NotEmpty();

            this.RuleFor(r => r.Description)
                .NotEmpty()
                .MaximumLength(500);
        }
    }

    internal sealed class AddTodoCommandHandler : AsyncRequestHandler<AddTodoCommand>
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly ISystemClock _systemClock;

        private readonly ISystemGuid _systemGuid;

        public AddTodoCommandHandler(
            IUnitOfWork unitOfWork,
            ISystemClock systemClock,
            ISystemGuid systemGuid)
        {
            this._unitOfWork = unitOfWork;
            this._systemClock = systemClock;
            this._systemGuid = systemGuid;
        }

        protected override async Task Handle(AddTodoCommand request, CancellationToken cancellationToken)
        {
            Todo todo = new Todo(
                this._systemGuid.Create(),
                request.Title,
                this._systemClock.EpochSeconds,
                request.Description);

            this._unitOfWork.Todos.Create(todo);

            await this._unitOfWork.SaveAsync(cancellationToken);
        }
    }
}
