﻿namespace CogentCode.FitnessDiary
{
    using CogentCode.FitnessDiary.Application;
    using CogentCode.FitnessDiary.Common.Constants;
    using CogentCode.FitnessDiary.Common.Extensions;
    using CogentCode.FitnessDiary.Common.Utilities;
    using CogentCode.FitnessDiary.Infrastructure.Database;
    using CogentCode.FitnessDiary.Presentation.Api;

    using Hellang.Middleware.ProblemDetails;

    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc.ApiExplorer;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;

    using Serilog;

    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            this.Configuration = configuration;
            this.Environment = environment;
        }

        private IConfiguration Configuration { get; }

        private IWebHostEnvironment Environment { get; }

        private DatabaseSettings DatabaseSettings => this.Configuration.GetSection(DatabaseSettings.Key).Get<DatabaseSettings>();

        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddCors();

            services
                .AddHealthChecks();

            services
                .AddRouting(RoutingSetup.Specification);

            services
                .AddSwaggerOptions()
                .AddSwaggerGen(SwaggerSetup.DocsSpecification)
                .AddSwaggerGenNewtonsoftSupport();

            services
                .AddProblemDetails(options => ProblemDetailsSetup.Specification(options, Environment))
                .AddRestApi()
                .AddControllers();

            services
                .AddApiVersioning(ApiVersioningSetup.Specification);

            services
                .AddVersionedApiExplorer(VersionedApiExplorerSetup.Specification);

            services
                .AddAuthentication();

            services
                .AddAuthorization();

            services
                .AddAutoMapperProfiles();

            services
                .AddDatabase(this.DatabaseSettings);

            services
                .AddApplication();

        }

        public void Configure(IApplicationBuilder app, IApiVersionDescriptionProvider provider)
        {
            app.UseProblemDetails();

            if (!Environment.IsDevelopment())
            {
                app.UseHsts();
            }

            app.UseSerilogRequestLogging();

            app.UseSwagger()
                .UseSwaggerUI(options => SwaggerSetup.UiSpecification(options, provider));

            app.UseHttpsRedirection();

            app.UseCors(options =>
                options
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader());

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
                endpoints.MapHealthChecks(HealthCheck.Endpoint);
            });
        }
    }
}
