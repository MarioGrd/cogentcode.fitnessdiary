﻿namespace CogentCode.FitnessDiary.Common.Utilities
{
    using Microsoft.AspNetCore.Routing;

    internal static class RoutingSetup
    {
        public static void Specification(RouteOptions options)
        {
            options.LowercaseUrls = true;
        }
    }
}