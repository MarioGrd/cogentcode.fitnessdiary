﻿namespace CogentCode.FitnessDiary.Common.Utilities
{
    using CogentCode.FitnessDiary.Common.Constants;

    using Microsoft.AspNetCore.Mvc.ApiExplorer;

    internal static class VersionedApiExplorerSetup
    {
        public static void Specification(ApiExplorerOptions options)
        {
            options.GroupNameFormat = Api.GroupNameFormat;
            options.SubstituteApiVersionInUrl = true;
        }
    }
}