﻿namespace CogentCode.FitnessDiary.Common.Utilities
{
    using CogentCode.FitnessDiary.Application.Contracts.Exceptions;

    using Hellang.Middleware.ProblemDetails;

    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Hosting;

    using System;
    using System.Linq;

    internal static class ProblemDetailsSetup
    {
        private static readonly Type[] KnownExceptions =
        {
            typeof(BadRequestException)
        };

        internal static void Specification(ProblemDetailsOptions options, IWebHostEnvironment enviroment)
        {
            options.IncludeExceptionDetails = (_, exception) =>
                enviroment.IsDevelopment()
                && !KnownExceptions.Contains(exception.GetType());

            options.Map<BadRequestException>(exception => new ValidationProblemDetails(exception.Errors)
            {
                Title = exception.Title,
                Detail = exception.Detail,
                Status = StatusCodes.Status400BadRequest
            });
        }
    }
}