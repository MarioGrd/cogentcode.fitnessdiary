﻿namespace CogentCode.FitnessDiary.Common.Utilities
{

    using CogentCode.FitnessDiary.Common.Constants;

    using Microsoft.AspNetCore.Http;

    using Serilog;
    using Serilog.AspNetCore;
    using Serilog.Events;

    using System;

    internal static class SerilogLoggingSetup
    {
        public static void Specification(RequestLoggingOptions options)
        {
            options.EnrichDiagnosticContext = EnrichFromRequest;
            options.GetLevel = ExcludeHealthChecks;
        }

        private static void EnrichFromRequest(IDiagnosticContext diagnosticContext, HttpContext httpContext)
        {
            HttpRequest request = httpContext.Request;

            diagnosticContext.Set(DiagnosticProperties.RequestContext.Host, request.Host);
            diagnosticContext.Set(DiagnosticProperties.RequestContext.Protocol, request.Protocol);
            diagnosticContext.Set(DiagnosticProperties.RequestContext.Scheme, request.Scheme);

            if (request.QueryString.HasValue)
            {
                diagnosticContext.Set(DiagnosticProperties.RequestContext.QueryString, request.QueryString);
            }

            Endpoint? endpoint = httpContext.GetEndpoint();

            if (endpoint is not null)
            {
                diagnosticContext.Set(DiagnosticProperties.RequestContext.EndpointName, endpoint.DisplayName);
            }
        }

        private static LogEventLevel ExcludeHealthChecks(HttpContext httpContext, double _, Exception exception)
        {
            if (IsError(httpContext))
            {
                return LogEventLevel.Error;
            }

            return IsHealthCheckEndpoint(httpContext)
                ? LogEventLevel.Verbose
                : LogEventLevel.Information;
        }

        private static bool IsError(HttpContext httpContext) =>
            httpContext.Response.StatusCode >= StatusCodes.Status500InternalServerError;

        private static bool IsHealthCheckEndpoint(HttpContext httpContext)
        {
            Endpoint? endpoint = httpContext.GetEndpoint();

            if (endpoint is null)
            {
                return false;
            }

            return string.Equals(endpoint.DisplayName, HealthCheck.DisplayName, StringComparison.Ordinal);
        }
    }
}