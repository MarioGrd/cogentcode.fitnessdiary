﻿namespace CogentCode.FitnessDiary.Common.Constants
{
    internal static class DiagnosticProperties
    {
        public static class RequestContext
        {
            public const string ContentType = nameof(ContentType);
            public const string EndpointName = nameof(EndpointName);
            public const string Host = nameof(Host);
            public const string Protocol = nameof(Protocol);
            public const string QueryString = nameof(QueryString);
            public const string Scheme = nameof(Scheme);
        }
    }
}