﻿namespace CogentCode.FitnessDiary.Common.Extensions
{
    using CogentCode.FitnessDiary.Common.Logging;

    using Serilog;
    using Serilog.Configuration;

    internal static class LoggingExtensions
    {
        public static LoggerConfiguration WithEscapedExceptionMessage(this LoggerEnrichmentConfiguration enrich)
        {
            return enrich.With<EscapedExceptionMessageEnricher>();
        }
    }
}