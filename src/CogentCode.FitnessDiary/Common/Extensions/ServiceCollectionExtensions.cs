﻿namespace CogentCode.FitnessDiary.Common.Extensions
{

    using CogentCode.FitnessDiary.Common.ApiDocumentation;

    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Options;

    using Swashbuckle.AspNetCore.SwaggerGen;

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    internal static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddAutoMapperProfiles(this IServiceCollection services)
        {
            IEnumerable<Assembly> assemblies = AppDomain.CurrentDomain
                .GetAssemblies()
                .Where(assembly => assembly?.FullName is not null && assembly.FullName.StartsWith("CogentCode.FitnessDiary."));

            services.AddAutoMapper(assemblies);

            return services;
        }

        public static IServiceCollection AddSwaggerOptions(this IServiceCollection services)
        {
            services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();

            return services;
        }
    }
}