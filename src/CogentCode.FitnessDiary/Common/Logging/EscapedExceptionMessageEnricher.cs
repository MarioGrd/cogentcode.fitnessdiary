﻿namespace CogentCode.FitnessDiary.Common.Logging
{

    using CogentCode.FitnessDiary.Common.Extensions;

    using Serilog.Core;
    using Serilog.Events;

    internal sealed class EscapedExceptionMessageEnricher : ILogEventEnricher
    {
        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            if (logEvent.Exception is null)
            {
                return;
            }

            LogEventProperty property =
                propertyFactory.CreateProperty(
                    name: "_Exception",
                    value: logEvent.Exception.ToString().EscapeNewLine());

            logEvent.AddPropertyIfAbsent(property);
        }
    }
}