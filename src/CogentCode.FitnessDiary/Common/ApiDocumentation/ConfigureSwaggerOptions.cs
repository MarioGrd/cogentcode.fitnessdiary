﻿namespace CogentCode.FitnessDiary.Common.ApiDocumentation
{
    using CogentCode.FitnessDiary.Common.Constants;

    using Microsoft.AspNetCore.Mvc.ApiExplorer;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Options;
    using Microsoft.OpenApi.Models;

    using Swashbuckle.AspNetCore.SwaggerGen;

    internal sealed class ConfigureSwaggerOptions : IConfigureOptions<SwaggerGenOptions>
    {
        private readonly IApiVersionDescriptionProvider _descriptionProvider;

        public ConfigureSwaggerOptions(IApiVersionDescriptionProvider descriptionProvider)
        {
            _descriptionProvider = descriptionProvider;
        }

        public void Configure(SwaggerGenOptions options)
        {
            foreach (ApiVersionDescription description in _descriptionProvider.ApiVersionDescriptions)
            {
                options.SwaggerDoc(
                    description.GroupName,
                    CreateOpenApiInfoForApiVersion(description));
            }
        }

        private static OpenApiInfo CreateOpenApiInfoForApiVersion(ApiVersionDescription description)
        {
            var openApiInfo = new OpenApiInfo()
            {
                Title = Api.Title,
                Version = description.ApiVersion.ToString(),
            };

            if (description.IsDeprecated)
            {
                openApiInfo.Description += " [DEPRECATED]";
            }

            return openApiInfo;
        }
    }
}