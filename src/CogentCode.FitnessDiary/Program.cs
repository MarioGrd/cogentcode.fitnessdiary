namespace CogentCode.FitnessDiary
{
    using CogentCode.FitnessDiary.Common.Constants;

    using Serilog;

    public class Program
    {
        public static int Main(string[] args)
        {
            var builder = new ConfigurationBuilder();

            BuildConfiguration(builder, args);

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(builder.Build())
                .CreateBootstrapLogger();

            try
            {
                Log.Information("Starting web host.");
                CreateHostBuilder(args).Build().Run();
                return ExitCode.Success;
            }
            catch (Exception exception)
            {
                Log.Fatal(exception, "Host terminated unexpectedly.");
                return ExitCode.Failure;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host
                .CreateDefaultBuilder(args)
                .UseSerilog((hostContext, loggerConfiguration) =>
                    loggerConfiguration.ReadFrom.Configuration(hostContext.Configuration)
                )
                .ConfigureWebHostDefaults(webBuilder =>
                    webBuilder.UseStartup<Startup>()
                );

        private static void BuildConfiguration(IConfigurationBuilder builder, string[] args)
        {
            var environment = Environment.GetEnvironmentVariable(HostEnviroment.Variable);

            var isDevelopment =
                string.IsNullOrWhiteSpace(environment)
                || string.Equals(environment, HostEnviroment.Development, StringComparison.OrdinalIgnoreCase);

            builder
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile(
                    path: "appsettings.json",
                    optional: false,
                    reloadOnChange: true)
                .AddJsonFile(
                    path: $"appsettings.{environment}.json",
                    optional: true,
                    reloadOnChange: true)
                .AddEnvironmentVariables();

            if (isDevelopment)
            {
                builder.AddUserSecrets<Program>();
            }

            builder.AddCommandLine(args);
        }
    }
}