﻿namespace CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database
{
    using CogentCode.FitnessDiary.Application.Contracts.Common;
    using CogentCode.FitnessDiary.Domain;

    public interface ITodoRepository
    {
        Task<Todo?> GetByIdAsync(Guid id, CancellationToken cancellationToken = default);

        Task<Todo> GetByIdSafeAsync(Guid id, CancellationToken cancellationToken = default);

        Task<List<Todo>> GetAsync(string? title, Page page, CancellationToken cancellationToken = default);

        void Create(Todo todo);

        void Update(Todo todo);

        void Delete(Todo todo);
    }
}