﻿namespace CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database
{
    using System.Threading;
    using System.Threading.Tasks;

    public interface IUnitOfWork
    {
        ITodoRepository Todos { get; }

        Task SaveAsync(CancellationToken cancellationToken = default);
    }
}