﻿namespace CogentCode.FitnessDiary.Application.Contracts.Common
{
    using System;

    public interface ISystemGuid
    {
        Guid Create() => Guid.NewGuid();
    }
}