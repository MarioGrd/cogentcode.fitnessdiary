﻿namespace CogentCode.FitnessDiary.Application.Contracts.Common
{
    public class Page
    {
        private const int DefaultPageIndex = 1;
        private const int DefaultPageSize = 20;

        public Page(
            int? pageNumber,
            int? pageSize)
        {
            this.PageNumber = !pageNumber.HasValue || pageNumber.Value < DefaultPageIndex ? DefaultPageIndex : pageNumber.Value;
            this.Take = !pageSize.HasValue || pageSize.Value < 0 ? DefaultPageSize : pageSize.Value;
        }

        public int PageNumber { get; }

        public int Take { get; }

        public int Skip
        {
            get => (this.PageNumber - 1) * this.Take;
        }
    }
}
