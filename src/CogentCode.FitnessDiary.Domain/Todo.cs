﻿namespace CogentCode.FitnessDiary.Domain
{
    public class Todo
    {
        public Guid Id { get; init; }

        public string Title { get; protected set; }

        public long Created { get; init; }

        public string Description { get; protected set; }

        public Todo(Guid id, string title, long created, string description)
        {
            this.Id = id;
            this.Title = title;
            this.Created = created;
            this.Description = description;
        }

        public void Update(string title, string description)
        {
            this.Title = title;
            this.Description = description;
        }
    }
}
